const selectors = {
  button: 'yt-icon-button#button.style-scope.ytd-notification-topbar-button-renderer.notification-button-style-type-default',
  popup: 'ytd-popup-container.style-scope.ytd-app',
  notificationContainer: 'iron-dropdown.style-scope.ytd-popup-container',
  notificationList: 'div#items.style-scope.yt-multi-page-menu-section-renderer',
  notificationSection: 'ytd-notification-renderer.style-scope.yt-multi-page-menu-section-renderer'
}

const notificationButton = document.querySelector(selectors.button);

const popupContainer = document.querySelector(selectors.popup);

let notificationContainer;
let notificationList;

const observePopup = (mutationsList, observer) => {
  if (!notificationContainer) {
    notificationContainer = document.querySelector(selectors.notificationContainer);
    // console.log('notificationContainer!');
    if (notificationContainer)
      notificationContainerObserver.observe(notificationContainer, { childList: true, subtree: true });
  }
}
const popupObserver = new MutationObserver(observePopup);

const observeNotificationContainer = (mutationsList, observer) => {
  if (!notificationList) {
    notificationList = popupContainer.querySelector(selectors.notificationList);
    // console.log('notificationList!');
  }
  
  if (notificationList) {
    observeNotificationList();
    notificationListObserver.observe(notificationList, { childList: true });
  }
};
const notificationContainerObserver = new MutationObserver(observeNotificationContainer);

const observeNotificationList = (mutationsList, observer) => {
  // console.log('notificationList observation');
  for(var section of notificationList.querySelectorAll(selectors.notificationSection)) {
    // console.log('section...');
    const addition = section.querySelector('.addition')
    if (!addition) {
      var div = document.createElement('div');
      div.classList.add('addition');
      div.innerHTML = 'AD';
      section.append(div);
      console.log('added!');
    }
  }
};
const notificationListObserver = new MutationObserver(observeNotificationList);

observePopup();
popupObserver.observe(popupContainer, { childList: true });

